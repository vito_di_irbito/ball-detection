#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <fstream>
#include "PixelToPixelAlgorithm.h"
//for testing
#include <time.h>
clock_t start;
int duration;

namespace enc = sensor_msgs::image_encodings;
using namespace std;
using namespace cv;

const int IMAGE_WIDTH = 400;
const int IMAGE_HEIGHT = 400;
const int EPS_Y = 5;
const string LEFT_WINDOW = "Ball Detection (Left Camera)";
const string RIGHT_WINDOW = "Ball Detection (Right Camera)";
vector<Vec3f> left_circles;
vector<Vec3f> right_circles;
Point left_center;
Point right_center;

Point circlesSearch(const sensor_msgs::ImageConstPtr& original_image, string WINDOW)
{
    // ROS image to OpenCV image
    cv_bridge::CvImagePtr cv_ptr;
    Mat src_image;
    Mat gray_image;
    Mat canny_image;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(original_image, enc::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("ballbot_gazebo::main.cpp::cv_bridge exception: %s", e.what());
        return Point(-1,-1);
    }
// testing
//src_image = cv_ptr->image;
    //resize(cv_ptr->image, src_image, Size(IMAGE_WIDTH, IMAGE_HEIGHT), 0, 0, INTER_NEAREST);
    resize(imread("test.jpg", 1), src_image, Size(IMAGE_WIDTH * 4, IMAGE_HEIGHT * 4), 0, 0, INTER_NEAREST);

    // search
    vector<Vec3f> circles;
    cvtColor(src_image, gray_image, CV_BGR2GRAY);
    GaussianBlur(gray_image, gray_image, Size(9, 9), 2, 2);
    Canny(gray_image, canny_image, 10, 100);

start = clock(); // test
    Point center = pixelToPixelAlgorithm(canny_image);
duration = (clock() - start) / (double)(CLOCKS_PER_SEC/1000); // test
ROS_INFO("eff alg time: %d", duration);
    circle(src_image, center, 3, cv::Scalar(0,0,255), -1, 8, 0);

/*cvtColor(cv_ptr->image, gray_image, CV_BGR2GRAY);
    GaussianBlur(gray_image, gray_image, Size(9, 9), 2, 2);*/
start = clock(); // test
    HoughCircles(canny_image, circles, CV_HOUGH_GRADIENT, 1,300,50, 10);
duration = (clock() - start) / (double)(CLOCKS_PER_SEC/1000); // test
ROS_INFO("hough alg time: %d", duration);

    // draw the circles detected
    for( size_t i = 0; i < circles.size(); i++ )
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
        int radius = cvRound(circles[i][2]);
        // center
        circle(src_image, center, 3, cv::Scalar(255,0,0), -1, 8, 0);
        // outline
        circle(src_image, center, radius, cv::Scalar(255,0,0), 3, 8, 0);
    }

    imshow(WINDOW, src_image);
    waitKey(3);

    if (WINDOW == LEFT_WINDOW)
        left_circles = circles;
    else
        right_circles = circles;
    return center;
}

void leftImageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
    left_center = circlesSearch(original_image, LEFT_WINDOW);
    /*for (int l = 0; l < left_circles.size(); l++)
        for (int r = 0; r < right_circles.size(); r++)
        {
            if (abs(left_circles[l][1] - right_circles[r][1]) < EPS_Y)
            {
                ROS_INFO("circle: y = %ld, left x = %ld, right x = %ld, left R = %ld, right R = %ld",
                    (long int)left_circles[l][1], (long int)left_circles[l][0], (long int)right_circles[r][0], (long int)left_circles[l][2], (long int)right_circles[r][2]);
            }
        }*/
    if (abs(right_center.y - left_center.y) < EPS_Y)
    {
        ROS_INFO("left circle: (%d,%d), right circle: (%d,%d)",left_center.x, left_center.y, right_center.x, right_center.y);
    }
}

void rightImageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
    right_center = circlesSearch(original_image, RIGHT_WINDOW);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ball_detection");
    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);
    namedWindow(LEFT_WINDOW, CV_WINDOW_AUTOSIZE);
    namedWindow(RIGHT_WINDOW, CV_WINDOW_AUTOSIZE);
    image_transport::Subscriber left_sub = it.subscribe("/ballbot/lcamera/image_raw", 1, leftImageCallback);
    image_transport::Subscriber right_sub = it.subscribe("/ballbot/rcamera/image_raw", 1, rightImageCallback);
    destroyWindow(LEFT_WINDOW);
    destroyWindow(RIGHT_WINDOW);
    ros::spin();
}
