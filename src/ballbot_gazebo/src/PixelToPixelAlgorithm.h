using namespace cv;
using namespace std;

static const int K = 5;

class Matrix
{
    int* matrix;

    public:
    int width;
    int height;

    public:
    Matrix(int _width, int _height)
    {
        width = _width;
        height = _height;
        matrix = (int*)malloc(sizeof(int) * width * height);
        clear();
    }

    ~Matrix()
    {
        free(matrix);
    }

    int get(int i, int j)
    {
        return matrix[i*width + j];
    }

    void set(int i, int j, int value)
    {
        matrix[i*width + j] = value;
    }

    void inc(int i, int j)
    {
        matrix[i*width + j]++;
    }

    void plotLine(Point p1, Point p2)
    {
        if (p1.x < 0)
            p1.x = 0;
        else if (p1.x >= width)
            p1.x = width - 1;
        if (p1.y < 0)
            p1.y = 0;
        else if (p1.y >= height)
            p1.y = height - 1;
        if (p2.x < 0)
            p2.x = 0;
        else if (p2.x >= width)
            p2.x = width - 1;
        if (p2.y < 0)
            p2.y = 0;
        else if (p2.y >= height)
            p2.y = height - 1;
    
        if (p1.x > p2.x)
        {
            int t = p1.x;
            p1.x = p2.x;
            p2.x = t;
            t = p1.y;
            p1.y = p2.y;
            p2.y = t;
        }
        inc(p1.x, p1.y);
        inc(p2.x, p2.y);
        int prevY = p1.y;
        int y;
        if (p1.x == p2.x)
        {
            if (p1.y < p2.y)
            {
                for (y = p1.y + 1; y < p2.y; y++)
                    inc(p1.x, y);
            }
            else
            {
                for (y = p2.y + 1; y < p1.y; y++)
                    inc(p1.x, y);
            }
            return;
        }
        float delta = (float)(p2.y - p1.y) / (float)(p2.x - p1.x);
        for (int x = p1.x + 1; x < p2.x; x++)
        {
            y = round((x - p1.x) * delta + p1.y);
            if (y - prevY > 1)
            {
            for (int u = prevY + 1; u < y; u++)    
                inc(x, u);
            }
            else if (prevY - y > 1)
            {
                for (int u = prevY - 1; u > y; u--)
                    inc(x, u);
            }
            inc(x, y);
            prevY = y;
        }
        if (p1.y < p2.y)
        {
            for (int u = prevY + 1; u < p2.y; u++)
                inc(p2.x, u);
        }
        else
        {
            for (int u = prevY - 1; u > p2.y; u--)
                inc(p2.x, u);
        }
    }

    void clear()
    {
        for (int i = 0; i < width * height; i++)
            matrix[i] = 0;
    }
};

bool out = true;
Point pixelToPixelAlgorithm(Mat canny_image)
{
    const uchar* image = canny_image.ptr();
    int step = (int)canny_image.step;
    int width = canny_image.cols;
    int height = canny_image.rows;

    Matrix matrix = Matrix(width, height);
    Matrix accumMatrix = Matrix(width, height);

    // binary edge map init
    for( int i = 0; i < height; i++ )
        for( int j = 0; j < width; j++ )
        {
            if( image[i * step + j] != 0 )
                matrix.set(j, i, 1);
        }
    // iterate blocks 2k+1
    int blockSize = 2*K + 1;
    for (int r = K; r < height - K; r++)
    {
        for (int c = K; c < width - K; c++)
        {
            if (matrix.get(c, r) == 0)
                continue; // not edge point
            int edgePointsCount = 0;
            for (int j = r - K; j <= r + K; j++)
            {
                for (int i = c - K; i <= c + K; i++)
                {
                    if (matrix.get(i, j) == 1)
                        edgePointsCount++;
                }
            }
            if (edgePointsCount != blockSize)
                continue;
            Point extremA = Point(-1,-1);
            int distanceA = -1;
            Point extremB = Point(-1,-1);
            int distanceB = -1;
            for (int j = r - K; j <= r + K; j++)
            {
                for (int i = c - K; i <= c + K; i++)
                {
                    if (matrix.get(i, j) == 1)
                    {
                        int d = abs(c - i) + abs(r - j);
                        bool sign = (c - i) * (c - extremA.x) < 0 || (r - j) * (r - extremA.y) < 0;
                        if (d >= distanceA)
                        {
                            if (sign)
                            {
                                distanceB = distanceA;
                                extremB = extremA;
                            }
                            distanceA = d;
                            extremA.x = i;
                            extremA.y = j;
                        }
                        else if (d > distanceB && sign)
                        {
                            distanceB = d;
                            extremB.x = i;
                            extremB.y = j;
                        }
                    }
                }
            }
            // check straight line
            Point relativeA = Point(extremA.x - c, extremA.y - r);
            Point relativeB = Point(extremB.x - c, extremB.y - r);
            if (abs(relativeB.y * (relativeB.x - relativeA.x) - relativeB.x * (relativeB.y - relativeA.y)) <= min(abs(relativeA.x - relativeB.x),abs(relativeA.y - relativeB.y)))
                continue;

            // equation of the line
            int coeffA = extremA.y - extremB.y;
            int coeffB = extremB.x - extremA.x;
            int coeffC = extremA.x * extremB.y - extremA.y * extremB.x;

            // concavity direction
            int D = coeffA * c + coeffB * r + coeffC;
            if (coeffB >= 0 && ((coeffA < 0 && D < 0) || (coeffA > 0 && D > 0)))
            {
                if (coeffB > 0)
                    accumMatrix.plotLine(Point(0,(-c * coeffB / coeffA) + r), Point(c,r));
                else if (coeffB == 0)
                    accumMatrix.plotLine(Point(0,r), Point(c,r));
            }
            else if (coeffB <= 0 && ((coeffA > 0 && D < 0) || (coeffA < 0 && D > 0)))
            {
                if (coeffB < 0)
                    accumMatrix.plotLine(Point(c,r), Point(width,(coeffB * (width - c) / coeffA) + r));
                else if (coeffB == 0)
                    accumMatrix.plotLine(Point(c,r), Point(width,r));
            }
            else if (coeffA <= 0 && ((coeffB > 0 && D > 0) || (coeffB < 0 && D < 0)))
            {
                if (coeffA < 0)
                    accumMatrix.plotLine(Point(c,r), Point((-r * coeffA / coeffB) + c,0));
                else if (coeffA == 0)
                    accumMatrix.plotLine(Point(c,r), Point(c,0));
            }
            else if (coeffA >= 0 && ((coeffB > 0 && D < 0) || (coeffB < 0 && D > 0)))
            {
                if (coeffA > 0)
                    accumMatrix.plotLine(Point(c,r), Point((coeffA * (height - r) / coeffB) + c,height));
                else if (coeffA == 0)
                    accumMatrix.plotLine(Point(c,r), Point(c,height));
            }
        }
    }

    // center search
    Point center = Point(-1,-1);
    int max = 0;
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            int current = accumMatrix.get(x,y);
            if (max < current)
            {
                max = current;
                center = Point(x, y);
            }
        }
    }

if (out)
{
ofstream file("matrix.txt");
for (int j = 0; j < height; j++)
{file << endl;
    for (int i = 0; i < width; i++)
            file << accumMatrix.get(i, j) << " ";
        }
out = false;
}
    return center;
}
